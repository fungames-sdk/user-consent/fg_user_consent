# User Consent

## Introduction

Most of FunGames modules requires to collect User Consent for tracking data or share information through the different SDKs.

## Integration Steps

1) **"Install"** or **"Upload"** FG User Consent plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

## Scripting API

> You can access to the consent status anywhere in your code by getting the following attributes :

```csharp
FGUserConsent.IsAttCompliant;
FGUserConsent.GdprStatus;
FGUserConsent.isGDPRCompliant;
FGUserConsent.IsIABCompliant;
FGUserConsent.ConsentString;
FGUserConsent.HasFullConsent;
FGUserConsent.Location;
```
> You can also subscribe to these callbacks if you want to attach custom actions at each step of the User Consent validation :

```csharp
FGUserConsent.OnComplete;

FGUserConsent.ATTCallbacks.Initialization;
FGUserConsent.ATTCallbacks.OnInitialized;
FGUserConsent.ATTCallbacks.Show;

FGUserConsent.GDPRCallbacks.Initialization;
FGUserConsent.GDPRCallbacks.OnInitialized;
FGUserConsent.GDPRCallbacks.Show;
FGUserConsent.GDPRCallbacks.OnValidated;
```